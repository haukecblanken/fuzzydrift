#load required python packages
import yaml
import pickle
from os import path,makedirs
import time
import numpy as np

#load required FuzzyDrift modules
from pkgs.pkg_forcing import forcing
from pkgs.pkg_utils import setup
from pkgs.pkg_model import run

#load experiment options file, listopt and read into dictionary for runtime usage, opt
optfile = './namelist.yaml'
with open(optfile,'r') as fid:
    listopt = yaml.safe_load(fid)

opt = {}
for k in listopt.keys():
    opt[k] = listopt[k]

#loop through parameter combinations to be tested and read parameters into opt
for force_type in listopt['force_type']:
    opt['force_type'] = force_type

    #set save directory according to forcing type
    opt['save_path'] = opt['save_path'].replace('XXX',force_type)
    makedirs(opt['save_path'],exist_ok=True)

    for dt_den in listopt['dt_den']:
        opt['dt_den'] = dt_den
        for res in listopt['res']:
            opt['res'] = res
            for npg in listopt['nPg']:
                opt['nPg'] = npg
                for npl in listopt['nPl']:
                    opt['nPl'] = npl
                    for fd in listopt['fractal_dimension']:
                        opt['fractal_dimension'] = fd
                        #skip any scenarios that mix crisp and fuzzy parameter values
                        if npg == 1 and not np.logical_and(npl == 1,fd == 1.):
                            continue
                        elif npl == 1 and not np.logical_and(npg == 1,fd == 1.):
                            continue
                        elif fd == 1. and not np.logical_and(npg == 1,npl == 1):
                            continue

                        #create file name based on parameter values and check if the output already exists
                        fname = 'fuzzyTrial_dt%1i_res%1i_npg%1i_npl%1i_fd%1i.pickle'%(dt_den,res*1000.,npg,npl,fd*10.)
                        savepath = path.join(opt['save_path'],fname)
                        #skip the simulation if the ouput already exists
                        if path.exists(savepath):
                            print('Using cached results at %s'%savepath)
                            continue

                        #initialize output dictionary, write, and loop through noise/membership levels
                        print('Running simulation with the following parameters (%s,%1i,%.2f,%1i,%1i,%.1f)...'%(force_type,dt_den,res,npg,npl,fd))
                        tic = time.perf_counter()
                        write = {}
                        for noise in opt['noise_levels']:
                            #skip any scenarios that mix crisp and fuzzy parameter values
                            if noise == 0. and not np.logical_and(npg == 1,np.logical_and(npl == 1,fd == 1.)):
                                continue
                            elif np.logical_and(npg == 1,np.logical_and(npl == 1,fd == 1.)) and noise != 0.:
                                continue

                            #set up initial inputs for model based on opt file
                            xy = setup().space_params(opt)
                            dt, N, time_vel = setup().time_params(opt)
                            global_offsets,local_offsets = setup().noise_params(opt,noise)

                            #generate analytical forcing field based on opt file
                            grid,u,v,spd = forcing().generate_forcing(opt,N)

                            #time step through simulation, and append xy with new positions and previous velocities at each time step
                            if __name__ == '__main__':
                                xy = run().time_step(opt,xy,dt,N,global_offsets,local_offsets,grid,u,v,time_vel)

                            #dump results into write
                            write[noise] = {'xy':xy,
                                            'dt':dt,
                                            'N':N,
                                            'grid':grid,
                                            'u':u,
                                            'v':v,
                                            'spd':spd,
                                            'time':time_vel,
                                            'offsets':{'global':global_offsets,'local':local_offsets},
                                            'opt':opt}

                        #complete simulation and save results
                        toc = time.perf_counter()
                        write['runtime'] = toc - tic
                        print(f"completed simulation in {toc-tic:0.4f} seconds")
                        print('Saving to %s'%savepath)
                        with open(savepath,'wb',-1) as fid:
                            pickle.dump(write,fid)
