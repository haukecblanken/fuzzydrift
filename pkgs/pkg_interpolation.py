import numpy as np

class IDW():
    """class of functions for interpolation with fuzzy-valued inputs

    Methods
    -------
    idw(grid,pts,val,offsets,t,search_radius=0.1,p=None)
        function for inverse distance weighted interpolation
    """

    def idw(self,grid,pts,val,offsets,t,search_radius = 0.1,p = None):
        """function to perform inverse distance weighted interpolation to a given set of points
        in a velocity field that contains an offset to account for uncertainty

        Uses the basic form of IDW, with weights (w) given as 1/d^p (d is the set of distances between the
        points to be interpolated to and the gridded values), and values interpolated onto a
        field x given as:

        x(pt)   = sum(wx)/sum(w) if all(d /= 0)
                = xi (nearest gridded point value) if any(d = 0)

        Parameters
        ----------
        grid: tuple
            3-element tuple containing the time, x, and y grid needed to describe the velocity field
            all elements are 3-dimensional numpy arrays of floats (time, x, and y)
        pts:
            3-column array describing positions to be interpolated to as [time,x,y]
        val: numpy array of floats
            3-dimensional numpy array containing values to be interpolated. Shape matches components of grid.
        offsets: numpy array of float
            offsets to be applied val field during interpolation
        t: int
            index of time dimension
        search_radius: float, optional (default 0.1)
            free parameter controlling the number of neighbouring grid points used in the interpolation, expressed in the same units
            as the spatial grid. Default is 0.1.
        p: float, optional (default None)
            power parameter for interpolation, set to the square of the number of dimensions (optimized for Blanken et al 2023)

        Returns
        -------
        out: numpy array of floats
            array containing the interpolated values at each point given by pts
        """

        #set the power parameter for the interpolation
        #set to be number of dimensions squared, somewhat arbitrary but found to optimize interpolation
        if p is None:
            p = float(len(grid)) * 2.

        #compute spatial distances between all points to be interpolated and the grid points
        ds = np.ones([len(pts),grid[1].shape[1],grid[2].shape[2]]) * 10**36.
        for ii in range(len(pts)):
            ds[ii,:,:] = np.sqrt((grid[1][t-1,:,:] - pts[ii][1])**2. + (grid[2][t-1,:,:] - pts[ii][2])**2.)
        #find all the grid points that have a particle within a distance search_radius to it
        ds = ds.min(axis = 0)
        j,i = np.where(ds <= search_radius)
        #truncate the field of values to be interpolated to the surrounding time steps and the grid points
        #with a particle within search_radius
        val = val[t-1:t+2,j,i]

        #compute scale factors required to non-dimensionalize grid
        scales = np.array([1. / (g.max() - g.min()) for g in grid])

        #compute the distance in all dimensions between the target points and the grid, d (used in the formulation of weights, weight = d^-p)
        d = np.array([np.sum(np.array([((g[t-1:t+2,j,i] - pt) * sc)**2. for g,pt,sc in zip(grid,pts[ip],scales)]),axis=0)**0.5 for ip in range(pts.shape[0])])
        #apply weights to get the output value at each target point. Offsets for uncertainty are added as a constant value to the truncated crisp field used
        #in the interpolation
        out = np.array([np.sum(d[ip]**(-1. * p) * (val + offsets[ip])) / np.sum(d[ip]**(-1. * p)) if not (d[ip] == 0.).any() else val[d[ip] == 0.][0] + offsets[ip] for ip in range(pts.shape[0])])

        return out
