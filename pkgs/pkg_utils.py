import numpy as np

from pkgs.pkg_forcing import perturbations

class setup():
    """utilities to convert namelist inputs to values used in the model

    Methods
    -------
    time_params(opt)
        returns the trajectory model time step, simulation length
    space_params(opt)
        returns the starting position and initial velocity
    noise_params(opt,noise)
        returns the global (absolute) and local (per-timestep) perturbation fields to be used in the simulation
    """

    def time_params(self,opt):
        """takes the number of revolutions and subdivisions of the circular velocity field from the namelist and converts
        them to required model time inputs

        Parameters
        ----------
        opt: dict
            dictionary of run-specific namelist parameters

        Returns
        -------
        dt: float
            model time step
        N: int
            number of time steps in the simulation
        time_vel: numpy array
            array of discrete times at which model is evaluated
        """

        dt = 2. * np.pi / opt['dt_den']
        #T is total simulation time, not needed outside of this function
        T = 2.**(opt['N_revolutions'] - 1) * 2. * np.pi
        N = int(T/dt)+1
        time_vel = np.linspace(0.,T,N)

        return dt, N, time_vel

    def space_params(self,opt):
        """creates list object containing start position and initial velocity

        Parameters
        ----------
        opt: dict
            dictionary of run-specific namelist parameters

        Returns
        -------
        xy: list
            list object containing a 1x2 numpy array containing the starting position
            as a complex number in the first position and the initial velocity
            (hardcoded to zero) as a complex number in the second position

            This list is appended to contain the new particle positions and previous
            particle velocities at each new time step.
        """

        xy = [np.array([[complex(opt['start_x'],opt['start_y']),complex(0.,0.)]])]

        return xy

    def noise_params(self,opt,noise,mode='uniform'):
        """generates global (absolute) and local (per-timestep) arrays of possible perturbing
        velocities based on specified noise level

        Parameters
        ----------
        opt: dict
            dictionary of run-specific namelist parameters
        noise: float
            absolute noise level specifying the strength of the perturbation
        mode: str, optional
            string specifying method by which to generate perturbations
            current options are 'uniform' and 'gaussian':
            -   'uniform' returns direction-independent perturbation with strength=noise, default
            -   'gaussian' returns speed and direction perturbations normally distributed around origin
                with speed standard deviation=noise

        Returns
        -------
        global_offsets: numpy array, type complex
            array of possible global (absolute) perturbation velocities
        local_offsets: numpy array, type complex
            array of possible local (per-timestep) perturbation velocities
        """

        global_offsets = perturbations().perturbation(opt['nPg'],noise,mode=mode)

        #TODO: generalize the derivation of local_noise, current form is setup-specific
        #(assumes length of simulation is 2\pi)
        local_noise = (1./opt['dt_den'])**(1./opt['fractal_dimension']) * noise
        local_offsets = perturbations().perturbation(opt['nPl'],local_noise,mode=mode)

        return global_offsets,local_offsets
