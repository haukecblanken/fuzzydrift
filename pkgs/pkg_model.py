import numpy as np
from scipy.spatial import ConvexHull
from scipy.interpolate import LinearNDInterpolator,griddata
from matplotlib.tri import Triangulation
import multiprocessing as mp
import time

from pkgs.pkg_interpolation import IDW

class functions():
    """class of functions needed to run the trajectory model

    Methods
    -------
    get_iterlen(i,global_offsets,local_offsets)
        function to determine the number of particles to be spawned per-time step from each starting location
    compute_possibility(n,i,u,v,grid,time_vel,xy1,dt,global_offsets,local_offsets)
        function to step particle tracking forward by one time step using Runge-Kutta 4th order, in a given crisp
        velocity field and offset with discretization information. Uses inverse distance weighting interpolation.
    parallel_possibility(i,u,v,grid,time_vel,xy1,dt,global_offsets,local_offsets,iterlen)
        wrapper to run compute_possibility in parallel over all particles for one time step (currently hardcoded
        to use 12 cores)
    serial_possibility(i,u,v,grid,time_vel,xy1,dt,global_offsets,local_offsets,iterlen)
        wrapper to run compute_possibility in series over all particles for one time step
    prepare_arrays(pos)
        processing utility for output from [serial,parallel]_possibility. Reformats new locations and velocity history
        and checks for invalid new positions
    offsets_positions(ptsa,nPg,xy)
        decimates new positions to eliminate those that will lead to expansion of membership function and prevent exponential
        growth of number of particles. Particle's velocity history is retained.
    """

    def get_iterlen(self,i,global_offsets,local_offsets):
        """function to determine the number of particles to be spawned per-time step from each starting location

        Parameters
        ----------
        i: int
            current time step of the simulation
        global_offsets: numpy array of complex
            set of possible global (absolute) velocity perturbations
        local_offsets: numpy array of complex
            set of possible local (per-timestep) velocity perturbations

        Returns
        -------
        iterlen: int
            number of particles to be spawned at each starting position for this time step
        """

        if i == 1:
            iterlen = len(global_offsets)
        else:
            iterlen = len(local_offsets)

        return iterlen

    def compute_possibility(self,n,i,u,v,grid,time_vel,xy1,dt,global_offsets,local_offsets):
        """function to step particle tracking forward by one time step using Runge-Kutta 4th order, in a given crisp
        velocity field and offset with discretization information. Uses inverse distance weighting interpolation.

        Parameters
        ----------
        n: int
            index of uncertainty discretization (length is given by iterlen)
        i: int
            current time step of the simulation
        u: numpy array of floats
            3-dimensional numpy array containing x-direction velocities
        v: numpy array of floats
            3-dimensional numpy array containing y-direction velocities
        grid: tuple
            3-element tuple containing the time, x, and y grid needed to describe the velocity field
            all elements are 3-dimensional numpy arrays of floats (time, x, and y)
        time_vel: numpy array
            array of discrete times at which model is evaluated
        xy1: 2-dimensional numpy array of complex numbers
            starting positions and velocity history of new particles
        dt: float
            model time step
        global_offsets: numpy array of complex
            set of possible global (absolute) velocity perturbations
        local_offsets: numpy array of complex
            set of possible local (per-timestep) velocity perturbations

        Returns
        -------
        xy2: 2-dimensional numpy array of complex numbers
            updated positions and velocity history of new particles
        """

        #formulate uncertainty component to be added to crisp solution
        #- for the start of the simulation (i == 1), the element of global_offsets corresponding to n
        #is added to each point in the crisp field
        #- for subsequent time steps the element of local_offsets corresponding to n is
        #added to the velocity history at each starting point
        if i == 1:
            offset = np.repeat(global_offsets[n],xy1.shape[0])
        else:
            offset = xy1[:,1] + local_offsets[n]

        #arrange the starting points into a 3-column array with format [time,x,y]
        #obtain the x,y-components of velocity at these points with uncertainty offsets applied (k1x,k1y)
        pts = np.column_stack([np.repeat(time_vel[i-1],xy1.shape[0]),xy1[:,0].real,xy1[:,0].imag])
        k1x = IDW().idw(grid,pts,u,offset.real,i)
        k1y = IDW().idw(grid,pts,v,offset.imag,i)

        #obtain estimates of first intermediate position based on k1x and k1y, arrange as [time,x,y]
        #and use this to interpolate for velocities at first intermediate position (k2x,k2y)
        pts = np.column_stack([np.repeat(time_vel[i-1] + dt/2.,xy1.shape[0]),xy1[:,0].real + 0.5*k1x*dt,xy1[:,0].imag + 0.5*k1y*dt])
        k2x = IDW().idw(grid,pts,u,offset.real,i)
        k2y = IDW().idw(grid,pts,v,offset.imag,i)

        #obtain estimates of second intermediate position based on k2x and k2y, arrange as [time,x,y]
        #and use this to interpolate for velocities at second intermediate position (k3x,k3y)
        pts = np.column_stack([np.repeat(time_vel[i-1] + dt/2.,xy1.shape[0]),xy1[:,0].real + 0.5*k2x*dt,xy1[:,0].imag + 0.5*k2y*dt])
        k3x = IDW().idw(grid,pts,u,offset.real,i)
        k3y = IDW().idw(grid,pts,v,offset.imag,i)

        #obtain estimate of final position based on k3x and k3y, arrange as [time,x,y],
        #and use this to interpolate for velocity at final point (k4x, k4y)
        pts = np.column_stack([np.repeat(time_vel[i-1] + dt,xy1.shape[0]),xy1[:,0].real + k3x*dt,xy1[:,0].imag + k3y*dt])
        k4x = IDW().idw(grid,pts,u,offset.real,i)
        k4y = IDW().idw(grid,pts,v,offset.imag,i)

        #update positions based on average of velocity estimates
        xx = xy1[:,0].real + (1./6.) * (k1x + 2.*k2x + 2.*k3x + k4x) * dt
        yy = xy1[:,0].imag + (1./6.) * (k1y + 2.*k2y + 2.*k3y + k4y) * dt

        #write updated positions as numpy array of complex, and record starting velocity including offset
        xy2 = np.column_stack([np.array([complex(xxx,yyy) for xxx,yyy in zip(xx,yy)]),offset])

        return xy2

    def parallel_possibility(self,i,u,v,grid,time_vel,xy1,dt,global_offsets,local_offsets,iterlen,nproc):
        """wrapper function to run timestepping for all discretized values of uncertain velocity in parallel

        Parameters
        ----------
        i: int
            current time step of the simulation
        u: numpy array of floats
            3-dimensional numpy array containing x-direction velocities
        v: numpy array of floats
            3-dimensional numpy array containing y-direction velocities
        grid: tuple
            3-element tuple containing the time, x, and y grid needed to describe the velocity field
            all elements are 3-dimensional numpy arrays of floats (time, x, and y)
        time_vel: numpy array
            array of discrete times at which model is evaluated
        xy1: 2-dimensional numpy array of complex numbers
            starting positions and velocity history of all particles to be tracked forward
        dt: float
            model time step
        global_offsets: numpy array of complex
            set of possible global (absolute) velocity perturbations
        local_offsets: numpy array of complex
            set of possible local (per-timestep) velocity perturbations
        nproc: int
            number of processors to use in computation

        Returns
        -------
        ptsa: 2-dimensional numpy array of complex numbers
            updated positions and velocity history of all new particles
        """

        with mp.Pool(processes=nproc) as pool:
            iterator = [pool.apply_async(self.compute_possibility,args=(n,i,u,v,grid,time_vel,xy1,dt,global_offsets,local_offsets,)) for n in range(iterlen)]
            ptsa = np.vstack([r.get() for r in iterator])

        return ptsa

    def serial_possibility(self,i,u,v,grid,time_vel,xy1,dt,global_offsets,local_offsets,iterlen):
        """wrapper function to run timestepping for all discretized values of uncertain velocity in serial

        Parameters
        ----------
        i: int
            current time step of the simulation
        u: numpy array of floats
            3-dimensional numpy array containing x-direction velocities
        v: numpy array of floats
            3-dimensional numpy array containing y-direction velocities
        grid: tuple
            3-element tuple containing the time, x, and y grid needed to describe the velocity field
            all elements are 3-dimensional numpy arrays of floats (time, x, and y)
        time_vel: numpy array
            array of discrete times at which model is evaluated
        xy1: 2-dimensional numpy array of complex numbers
            starting positions and velocity history of all particles to be tracked forward
        dt: float
            model time step
        global_offsets: numpy array of complex
            set of possible global (absolute) velocity perturbations
        local_offsets: numpy array of complex
            set of possible local (per-timestep) velocity perturbations

        Returns
        -------
        ptsa: 2-dimensional numpy array of complex numbers
            updated positions and velocity history of all new particles
        """

        ptsa = np.vstack([self.compute_possibility(n,i,u,v,grid,time_vel,xy1,dt,global_offsets,local_offsets) for n in range(iterlen)])

        return ptsa

    def prepare_arrays(self,ptsa):
        """script to discard invalid new positions from time stepping result

        Parameters
        ----------
        ptsa: 2-dimensional numpy array of complex numbers
            updated positions and velocity history of all new particles

        Returns
        -------
        ptsa: 2-dimensional numpy array of complex numbers
            updated positions and velocity history of all new particles,
            with invalid positions removed
        no_point_flag: bool
            flag to indicate if all new positions are invalid
        """
        c = np.where(np.isnan(ptsa[:,0]))[0]
        if len(c) > 0:
            ptsa = np.delete(ptsa,c,axis=0)
        no_point_flag = len(ptsa) == 0

        return ptsa, no_point_flag

    def offsets_positions(self,ptsa,nPg,xy):
        """function to decimate number of particles to only retain extreme positions (which add useful
        information to the membership function), and to record the velocity history of the particles in
        extreme positions

        Parameters
        ----------
        ptsa: 2-dimensional numpy array of complex numbers
            updated positions and velocity history of all new particles,
            with invalid positions removed
        nPg: int
            number of global particles, used to discretize the membership function of position and
            decimate particles according to this discretization
        xy: list
            list of decimated ptsa arrays from previous time steps

        Returns
        -------
        xy: list
            updated list of decimated ptsa arrays, including current time step
        """

        #create rectangular grid with nPg^2 elements to cover particle positions
        npts = nPg**2.
        Lx = ptsa[:,0].real.max() - ptsa[:,0].real.min()
        Ly = ptsa[:,0].imag.max() - ptsa[:,0].imag.min()
        if nPg > 1:
            Nx = int(np.sqrt(npts) * Lx / (Lx + Ly))
            Ny = int(np.sqrt(npts)) - Nx
        else:
            Nx = 1
            Ny = 1

        gx = np.linspace(ptsa[:,0].real.min(),ptsa[:,0].real.max(),Nx)
        gy = np.linspace(ptsa[:,0].imag.min(),ptsa[:,0].imag.max(),Ny)
        x,y = np.meshgrid(gx,gy)

        #interpolate new particle positions to grid using nearest neighbour interpolation
        z = griddata((ptsa[:,0].real,ptsa[:,0].imag),np.ones_like(ptsa[:,0].real),(x,y),method='nearest',fill_value=0.)
        #find the indices of the grid points containing particles
        j,i = np.where(z == 1.)
        #find the closest particle to each grid point containing particles, and keep only these
        inds = [np.where(np.sqrt((ptsa[:,0].real - x[jj,ii])**2. + (ptsa[:,0].imag - y[jj,ii])**2.) == np.sqrt((ptsa[:,0].real - x[jj,ii])**2. + (ptsa[:,0].imag - y[jj,ii])**2.).min())[0][0] for jj,ii in zip(j,i)]
        inds = np.unique(np.asarray(inds)).tolist()

        #update the list of particle positions and velocity history
        xy.append(ptsa[inds,:])

        return xy

class run():
    """class of higher level functions to control trajectory model run

    Methods
    -------
    time_step(opt,xy,dt,N,global_offsets,local_offsets,grid,u,v,time_vel)
        function to complete time stepping given forcing, uncertainty, and initial conditions
    """

    def time_step(self,opt,xy,dt,N,global_offsets,local_offsets,grid,u,v,time_vel):
        """function to complete time stepping given forcing, uncertainty, and initial conditions

        Parameters
        ----------
        opt: dict
            dictionary of run-specific namelist parameters
        xy: list
            single-member list containing starting position and initial velocity
        dt: float
            model time step
        N: int
            number of time steps in the simulation
        global_offsets: numpy array of complex
            set of possible global (absolute) velocity perturbations
        local_offsets: numpy array of complex
            set of possible local (per-timestep) velocity perturbations
        grid: tuple
            3-element tuple containing the time, x, and y grid needed to describe the velocity field
            all elements are 3-dimensional numpy arrays of floats (time, x, and y)
        u: numpy array of floats
            3-dimensional numpy array containing x-direction velocities
        v: numpy array of floats
            3-dimensional numpy array containing y-direction velocities
        time_vel: numpy array
            array of discrete times at which model is evaluated

        Returns
        -------
        xy: list
            list of numpy arrays (type complex) containing particle positions and velocity histories
            for all time steps
        """

        #initialize while loop through time steps, starting with t=1
        i = 1
        while i < N:
            #get the number of points in the discretization of uncertainty for this time step
            iterlen = functions().get_iterlen(i,global_offsets,local_offsets)

            #conduct time stepping
            if opt['parallel']:
                pos = functions().parallel_possibility(i,u,v,grid,time_vel,xy[-1],dt,global_offsets,local_offsets,iterlen,opt['nproc'])
            else:
                pos = functions().serial_possibility(i,u,v,grid,time_vel,xy[-1],dt,global_offsets,local_offsets,iterlen)

            #eliminate invalid points, and stop simulation if no valid points are found
            ptsa, no_point_flag = functions().prepare_arrays(pos)
            if no_point_flag:
                print('No more points in domain. Stopping time stepping at step %1i.'%i)
                return xy
            #decimate grid to maintain manageable numbers of particles
            xy = functions().offsets_positions(ptsa,opt['nPg'],xy)

            i += 1

        return xy
