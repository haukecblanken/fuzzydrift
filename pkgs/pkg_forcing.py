import numpy as np
from matplotlib.path import Path
from scipy.spatial import ConvexHull

class forcing():
    """scripts to generate analytical forcing fields for testing trajectory modelling scheme

    Methods
    -------
    generate_forcing(opt,N)
        script to generate grid and forcing fields for Blanken et al 2023 paper
        'constant' is the steady field
        'wave' is the unsteady field
    """

    def generate_forcing(self,opt,N):
        """script to generate grid and forcing fields for Blanken et al 2023 paper

        Parameters
        ----------
        opt: dict
            dictionary of run-specific namelist parameters
        N: int
            number of time steps in the simulation

        Returns
        -------
        grid: tuple
            3-element tuple containing the time, x, and y grid needed to describe the velocity field
            all elements are 3-dimensional numpy arrays of floats (time, x, and y)
        u: numpy array of floats
            3-dimensional numpy array containing x-direction velocities
        v: numpy array of floats
            3-dimensional numpy array containing y-direction velocities
        spd: numpy array of floats
            3-dimensional numpy array containing speeds
        """

        #generate space and time grids based on namelist parameters 'extent' and 'res', as well as number of time steps
        tg,xg,yg = np.meshgrid(np.linspace(0.,2.*np.pi,N),
                               np.arange(-opt['extent'],opt['extent'] + opt['res'] / 2.,opt['res']),
                               np.arange(-opt['extent'],opt['extent'] + opt['res'] / 2.,opt['res']),indexing='ij')
        #convert space grid to polar coordinates
        r = np.sqrt(xg**2. + yg**2.)
        theta = np.arctan2(yg,xg) + np.pi/2.

        if opt['force_type'] == 'constant':
            #steady forcing in Blanken et al 2023
            spd = np.sin(np.pi * r / opt['extent']) * opt['amp']
        elif opt['force_type'] == 'wave':
            #unsteady forcing in Blanken et al 2023
            spd = np.cos(tg + 0.5) * np.sin(np.pi * r / opt['extent']) * opt['amp']
        elif opt['force_type'] == 'pulse':
            #square-wave pulse forcing orbiting a circular velocity field, not used in Blanken et al 2023
            #TODO: this forcing type doesn't yet seem to work correctly. Figure out why that is.
            T = 1. * np.pi
            tau = np.pi / 4.
            spd = np.array([[[opt['amp']*tau/T + (2.*opt['amp']/np.pi) * np.sum([np.sin(np.pi*n*tau/T + k) * np.cos(2.*np.pi*n*tg[k,j,i]/T + k) / n for n in range(1,51)]) for i in range(tg.shape[2])] for j in range(tg.shape[1])] for k in range(tg.shape[0])]) * np.sin(np.pi * r / opt['extent']) * opt['amp']

        #convert forcing to u,v-coordinates
        u = spd * np.cos(theta)
        v = spd * np.sin(theta)

        return (tg,xg,yg),u,v,spd

class perturbations():
    """scripts to generate set of possible perturbations to crisp velocity field based
    on inputs from opt file

    Methods
    -------
    perturbation(nP,mag,mode)
        returns a numpy array of complex numbers describing the possible velocity perturbations
    """

    def perturbation(self,nP,mag,mode='uniform'):
        """generates either uniformly or normally distributed forcing perturbation sets based in input
        magnitude

        Parameters
        ----------
        nP: int
            number of 'particles' controlling the discretization of uncertainty
        mag: float
            absolute noise level specifying the strength of the perturbation
        mode: str, optional
            string specifying method by which to generate perturbations
            current options are 'uniform' and 'gaussian':
            -   'uniform' returns direction-independent perturbation with strength=noise, default
            -   'gaussian' returns a perturbation normally distributed in speed and direction around origin
                with standard deviation=noise

        Returns
        -------
        outvels: numpy array, type complex
            array of complex numbers describing possible perturbations to the velocity field for the given mode
            and input magnitude

        Raises
        ------
        ValueError
            if an invalid mode is specified (must be 'uniform' or 'gaussian')
        """
        #return no perturbation for crisp simulations
        if mag == 0.:
            return np.array([np.complex(0.,0.)])

        #generate speed, rad, and direction, theta, perturbations from mag and mode
        if mode == 'uniform':
            #speed perturbation is uniform and all directions are considered, i.e. isotropic
            rad = mag
            theta = np.linspace(0.,2.*np.pi,nP)
        elif mode == 'gaussian':
            #generate two random numbers, one to perturb speed and one to perturb direction
            r1 = np.random.randn(nP)
            r2 = np.random.randn(nP)

            #apply speed perturbation scaled by user-input noise magnitude
            rad = r1 / np,abs(r1).max() * mag
            #apply direction perturbation, in radians
            theta = r2 / np.abs(r2).max() * 2. * np.pi
        else:
            raise ValueError('Please specify valid perturbation mode (uniform or gaussian)')

        #convert speed and direction to orthogonal components, u and v
        u = rad * np.cos(theta)
        v = rad * np.sin(theta)
        vps = np.column_stack([u,v])

        #draw a line/ConvexHull around the perturbations in u-v space
        hull = ConvexHull(vps)
        verts = vps[hull.vertices,:]; verts = np.vstack([verts,verts[0,:]])
        bb = Path(verts)

        #map the component-wise perturbations onto an nPxnP grid by checking if the grid points are in the convex hull
        ugrid = np.hstack([np.linspace(-np.max(np.abs(u)),0.,int(np.ceil(nP/2))),
                            np.linspace(0.,np.max(np.abs(u)),int(np.floor(nP/2)))[1:]])
        vgrid = np.hstack([np.linspace(-np.max(np.abs(v)),0.,int(np.ceil(nP/2))),
                            np.linspace(0.,np.max(np.abs(v)),int(np.floor(nP/2)))[1:]])

        #arrange the gridded perturbations into a 1-dimensional numpy array of complex numbers
        outvels = np.array([]).astype(complex)
        for j in range(len(ugrid)):
            for i in range(len(vgrid)):
                if bb.contains_point((ugrid[j],vgrid[i])):
                      outvels = np.hstack([outvels,complex(ugrid[j],vgrid[i])])

        return outvels
