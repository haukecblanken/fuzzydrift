#this isn't really used....

import matplotlib.pyplot as plt
from matplotlib.path import Path
import cmocean.cm as cm

class basic():
    def plot_forcing(self,s,xg,yg,u,v,spd):
        fig,ax = plt.subplots(figsize=(7,6))
        pc = ax.pcolormesh(xg[0,:,:],yg[0,:,:],spd[0,:,:],cmap=cm.balance,alpha=0.5)
        plt.colorbar(pc,shrink=0.7,label='Speed')
        ax.quiver(xg[0,::s,::s],yg[0,::s,::s],u[0,::s,::s],v[0,::s,::s],scale=30.)
        plt.show()

    def get_edge_paths(self,data):
        paths = {}
        for noise in data.keys():
            paths[noise] = {}
            for t in range(len(data[noise]['xy'])):
                closeside = []
                farside = []
                tsort = []
                x = data[noise]['xy'][t][:,0].real
                y = data[noise]['xy'][t][:,0].imag
                thres = noise * np.std(np.sqrt(np.diff(x)**2. + np.diff(y)**2.))
                for theta in np.linspace(0.,2.*np.pi,360):
                    xp = x * np.cos(theta) - y * np.sin(theta)
                    yp = x * np.sin(theta) + y * np.cos(theta)
                    c = np.where(np.logical_and(xp > 0.,np.abs(yp) < thres))[0]
                    if len(c) == 0:
                        continue
                    else:
                        r = np.sqrt(x[c]**2. + y[c]**2.)
                        p1 = np.array([x[c][r == r.min()],y[c][r == r.min()]])
                        p2 = np.array([x[c][r == r.max()],y[c][r == r.max()]])
                        closeside.append(p1)
                        farside.append(p2)
                        if theta <= np.pi:
                            tsort.append(theta)
                        else:
                            tsort.append(theta - 2 * np.pi)
                sorting = np.asarray(tsort).argsort()
                closeside = np.asarray(closeside)[sorting]
                farside = np.asarray(farside)[sorting]
                pts = np.vstack([closeside,farside[::-1,:]])
                bb = Path(pts)

                paths[noise][t] = {'pts':pts,'path':bb}

        return path
